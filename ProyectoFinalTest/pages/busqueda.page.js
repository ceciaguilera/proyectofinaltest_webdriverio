const basePage = require('../pages/base.page');

 class BusquedaPage {

    //Elementos Web
    get resultado(){ return $('h5') }
    get logo(){ return $('h5') }


    //-------------------------------------------------------------------------------------------------------//
  
    /**
     * Click en el resultado de la búsqueda
     */
    ingresarAlResultado() {
        basePage.clickOnElement(this.resultado);
    }

    /**
     * Obtener texto del resultado de la búsqueda
     */
    obtenerNombreResultado() {
        addStep('Obtener resultado')
        return this.resultado.getText();
    }

 }

 module.exports = new BusquedaPage();