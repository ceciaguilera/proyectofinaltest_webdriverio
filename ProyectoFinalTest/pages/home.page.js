const basePage = require('../pages/base.page');
 class HomePage {

    //WebElements
    get barraDeBusqueda(){ return $('[name="search_query"]') }
    get menuTshirt(){ return $('//*[@id="block_top_menu"]/ul/li[3]/a') }
    get logoWeb(){ return $('#header_logo') }
    get cart(){ return $('//*[@id="header"]/div[3]/div/div/div[3]/div/a') }
    get cartEmpty(){ return $('[class="alert alert-warning"]') }
    get resultadoCartEmpy(){ return $('[class="alert alert-warning"]') }

    get contactUs(){ return $('//*[@id="contact-link"]/a') }
    get barraEmail(){ return $('#email') }
    get resultadoBlank(){ return $('[class="alert alert-danger"]') }

    get dropDownHeading(){ return $('#uniform-id_contact') }

    get menu(){ return $('#block_top_menu')}
    

    
    //-------------------------------------------------------------------------------------------------------//

    /**
     * Escribe el artículo en el campo de búsqueda y presiona Enter
     * @param {String} articulo que se buscará
     */
    clickContactUs(){
        addStep('Obtener Contact Us')
        basePage.clickearElemento(this.contactUs)
    }

    buscar(articulo) {
        addStep(`Buscar artículo: ${articulo}`)
        basePage.clearAndSendKeys(this.barraDeBusqueda, articulo);
        this.barraDeBusqueda.keys('Enter');
    }

    escribirEmail(email) {
        addStep('Escribir Email')
        basePage.clearAndSendKeys(this.barraEmail, email);
        this.barraEmail.keys('Enter');
    }

    clickMenu(){
        addStep('Obtener menú')
        basePage.clickearElemento(this.menuTshirt)
    }

    clickLogo(){
        addStep('Obtener Logo')
        basePage.clickearElemento(this.logoWeb)
    }

    clickCart(){
        addStep('Obtener Carro')
        basePage.clickearElemento(this.cart)
    }

    /**
     * Obtener texto de la barra de búsqueda
     */
    obtenerTextoBusqueda() {
        addStep('Obtener texto de la barra de búsqueda')
        return this.barraDeBusqueda.getValue();
    }

    obtenerLogoResultado() {
        addStep('Obtener resultado')
        return this.logoWeb.getText();
    }

    obtenerTextoCarroVacio() {
        addStep('Obtener resultado')
        return this.cartEmpty.getText();
    }

    obtenerNombreResultadoCarroVacio() {
        addStep('Obtener resultado')
        return this.resultadoCartEmpy.getText();
    }

 }

 module.exports = new HomePage();